# exim4-backport

Using Gitlab CI to backport Exim4 packages from Debian Testing to Debian Stable.

## Description
This is a Gitlab CI job triggered by a daily schedule. It pulls the source for Exim from Debian Bookworm (testing) and builds it on Debian Bullseye/11 (stable).

The CI artifacts are the deb packages built by `dpkg-buildpackage`.

## Background
I needed Exim 4.95's [support for Sender Rewrite Scheme](https://exim.org/exim-html-4.95/doc/html/spec_html/ch-dkim_spf_srs_and_dmarc.html#SECTSRS), and Debian 11 comes with 4.94.

## Future Work
* Use [Gitlab's package registry](https://gitlab.com/davepage_mcr/exim4-backport/-/packages) to serve the CI artifacts. Hopefully this'll just give us a URL to stick in sources.list.
* Test the packages before upload - install them on Debian 11, and perform some testing (starting with "is the process running and listening on port 25?")
* I didn't want to make an official [Debian Backport](https://backports.debian.org/) because of the maintenance overhead, but if this CI job can do most of the work for me, I may consider doing that 
